#file_share_into_sql_database.py
#created by _KUL (Kuleshov)
#version 2018-02-06
#Apache License 2.0

import os
import sys
import datetime
import psycopg2
import psycopg2.extras
import time

#подключаемся к базе
try:
    conn = psycopg2.connect("dbname='shareinfotosql' user='shareinfotosql' password='shareinfotosql9' host='10.1.1.1' port='5432'")
except:
    print("Ошибка подключения к базе данных!")
    sys.exit(1)
#cur = conn.cursor()
cur_dict = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

logf = open("exception.log", "w")

try:
    #служебные переменные
    services = {
        'time':{ #время начала и окончания работы
            'start':datetime.datetime.now(),
            'stop':None
        },
        'countfiles':{ #подсчёт обработанных файлов
            'add':0,
            'update':0,
            'delete':0
            },
        'id':None, #id текущей обработки
        'sizefiles':{ #подсчёт изменения объёма данных
            'add':0,
            'update':0,
            'delete':0
            },
        'paths':{}, #словарь корневых каталогов для поиска
        'types':{}, #типы файлов
        'filesindb':{}, #поиск файлов в базе относительно текущего обрабатываемого каталога
        'fileinfs':{}, #информация о файле в ФС для записи в БД
        'deleteafterdays':90, #автоочистка базы от старых записей через X дней
        'types': {}, #множество типов файлов
        'updatefiles':[], #кеш-список файлов для обновления информации в базе
        'insertfiles':[], #кеш-список файлов для добавления в базу
        }

    #актуальный список расширений файлов. добавляем элементы при отсутствии
    def mytypes(name=None):
        def mytypesget():
            cur_dict.execute("SELECT name,id FROM types")
            services['types'].clear
            for row in cur_dict:
                services['types'].update({row[0]:row[1]})
        if name == None:
            mytypesget()
            return services['types']
        if not bool(services['types']):
            mytypesget()
        if name not in services['types']:
            cur_dict.execute("INSERT INTO types (name) VALUES (%s)",
                (name.lower(),))
            conn.commit()
            mytypesget()
        return services['types'][name]

    #фиксируем начало анализа
    cur_dict.execute("INSERT INTO analysis (timestart) VALUES (%s) RETURNING id, timestart",
        (services['time']['start'],))
    services['id'] = cur_dict.fetchone()['id']
    conn.commit()

    #выбираем и перебираем корневые каталоги
    cur_dict.execute("SELECT id,path FROM paths")
    services['paths'] = dict(cur_dict.fetchall())
    for paths_id in services['paths']:
        path = '\\\\?\\'+services['paths'][paths_id] #добавляем UNC. избегаем ограничение Winodws пути в 260 символов
        path = os.walk(path)

        #перебираем каталоги в глубь
        for root, dirs, files in path:
            services['filesindb'].clear()
            cleanroot = str(root[4:]) #в базе храним чистый путь, без UNC
            cur_dict.execute("""
                                SELECT concat(files.name::text, types.name::text) as fullname, files.name as filename, files.id, 
                                    files.types_id, files.folder, files.createtime, types.name as typename
                                FROM files, types 
                                WHERE files.types_id = types.id AND paths_id = %s AND folder = %s AND deleted_analysis_id IS NULL
                            """, (paths_id,cleanroot,))

            #получаем данные о файлах каталога из БД
            for row in cur_dict:
                services['filesindb'][row['fullname']] = dict(row)

            #чистим кеш перед обработкой файлов
            services['updatefiles'].clear()
            services['insertfiles'].clear()

            #перебираем фактический список файлов из ФС
            for file in files:
                services['fileinfs'] = dict()
                services['fileinfs']['fullname'] = os.path.splitext(file)[0]+os.path.splitext(file)[1].lower()
                services['fileinfs']['name'] = os.path.splitext(file)[0]
                services['fileinfs']['types_id'] = mytypes(os.path.splitext(file)[1].lower())
                services['fileinfs']['types'] = os.path.splitext(file)[1].lower()
                services['fileinfs']['folder'] = cleanroot
                services['fileinfs']['paths_id'] = paths_id
                services['fileinfs']['size'] = os.path.getsize(os.path.join(root,file))
                try:
                    services['fileinfs']['createtime'] = datetime.datetime.fromtimestamp(os.stat(root+'\\'+file).st_ctime)
                except:
                    services['fileinfs']['createtime'] = datetime.datetime(1970, 1, 1, 0, 0)
                try:
                    services['fileinfs']['modifitime'] = datetime.datetime.fromtimestamp(os.stat(root+'\\'+file).st_mtime)
                except:
                    services['fileinfs']['modifitime'] = datetime.datetime(1970, 1, 1, 0, 0)
                services['fileinfs']['services_id'] = services['id']

                #принимем решение о обновлении информации или занесении новой записи
                if services['fileinfs']['fullname'] in services['filesindb'] \
                and services['fileinfs']['createtime'] == services['filesindb'][services['fileinfs']['fullname']]['createtime']:
                    services['fileinfs']['id'] = services['filesindb'][services['fileinfs']['fullname']]['id']
                    services['updatefiles'].append(services['fileinfs'])
                    services['countfiles']['update'] += 1
                    services['sizefiles']['update'] += services['fileinfs']['size']
                else:
                    services['insertfiles'].append(services['fileinfs'])
                    services['countfiles']['add'] += 1
                    services['sizefiles']['add'] += services['fileinfs']['size']

            #обновляем информацию о файлах в БД
            if len(services['updatefiles']) > 0:
                cur_dict.executemany("""
                                        UPDATE files SET size = %(size)s, modifitime = %(modifitime)s, indexed_analysis_id = %(services_id)s
                                        WHERE id = %(id)s
                                    """,services['updatefiles'])

            #добавляем информацию о новых файлах в БД
            if len(services['insertfiles']) > 0:
                cur_dict.executemany("""
                                        INSERT INTO files (createtime, folder, found_analysis_id, indexed_analysis_id,
                                            modifitime, name, paths_id, size, types_id)
                                        VALUES (%(createtime)s, %(folder)s, %(services_id)s, %(services_id)s,
                                            %(modifitime)s, %(name)s, %(paths_id)s, %(size)s, %(types_id)s)
                                    """,services['insertfiles'])
            conn.commit()

    #помечаем все не найденные файлы, данным анализом, как удалённые
    cur_dict.execute("""
                    SELECT count(id) as count, sum(size) as sum
                    FROM files
                    WHERE indexed_analysis_id <> %s AND deleted_analysis_id IS NULL
                """, (services['id'],))
    tmp = dict(cur_dict.fetchone())
    services['countfiles']['delete'] = tmp['count']
    services['sizefiles']['delete'] = tmp['sum']

    cur_dict.execute("""
                        UPDATE files SET deleted_analysis_id = %s
                        WHERE indexed_analysis_id <> %s AND deleted_analysis_id IS NULL
                    """, (services['id'],services['id']))
    conn.commit()

    print(services['countfiles'])
    print(services['sizefiles'])

    #чистка БД от ненужных записей
    #удаляем истёкшую историю хранения файлов
    cur_dict.execute("""
                        DELETE FROM files
                        WHERE deleted_analysis_id IN 
                        (SELECT analysis.id FROM analysis 
                            WHERE files.deleted_analysis_id = analysis.id and analysis.timestart < (now() - INTERVAL '%s day'))
                    """, (services['deleteafterdays'],))
    #удаляем неиспользуемую истёкшую историю анализа
    cur_dict.execute("""
                        DELETE FROM analysis WHERE analysis.timestart < (now() - INTERVAL '%s day') 
                        AND id NOT IN (SELECT files.deleted_analysis_id FROM files,analysis 
                                                WHERE files.deleted_analysis_id = analysis.id
                                            UNION
                                                SELECT files.found_analysis_id FROM files,analysis  
                                                WHERE files.found_analysis_id = analysis.id
                                            UNION
                                                SELECT files.indexed_analysis_id FROM files,analysis  
                                                WHERE files.indexed_analysis_id = analysis.id);
                    """, (services['deleteafterdays'],))
    conn.commit()

    #фиксируем результат текущего анализа
    services['time']['stop'] = datetime.datetime.now()
    cur_dict.execute("""
                        UPDATE analysis SET 
                            timestop = %s,
                            countadd = %s, countdelete = %s, countupdate = %s,
                            sizeadd = %s, sizedelete = %s, sizeupdate = %s
                        WHERE id = %s
                    """, (services['time']['stop'],
                            services['countfiles']['add'], services['countfiles']['delete'], services['countfiles']['update'],
                            services['sizefiles']['add'], services['sizefiles']['delete'], services['sizefiles']['update'],
                            services['id']
                        ))
    conn.commit()
    cur_dict.close()
    conn.close()
except Exception as e:
    logf.write(str(e))