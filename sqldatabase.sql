--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-1)
-- Dumped by pg_dump version 10.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: shareinfotosql; Type: COMMENT; Schema: -; Owner: shareinfotosql
--

COMMENT ON DATABASE shareinfotosql IS 'Инвентаризация и анализ файлов в файловой шаре';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: analysis; Type: TABLE; Schema: public; Owner: shareinfotosql
--

CREATE TABLE analysis (
    id bigint NOT NULL,
    timestart timestamp without time zone NOT NULL,
    timestop timestamp without time zone,
    countadd bigint,
    countdelete bigint,
    countupdate bigint,
    sizeadd bigint,
    sizedelete bigint,
    sizeupdate bigint
);


ALTER TABLE analysis OWNER TO shareinfotosql;

--
-- Name: analysis_id_seq; Type: SEQUENCE; Schema: public; Owner: shareinfotosql
--

CREATE SEQUENCE analysis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE analysis_id_seq OWNER TO shareinfotosql;

--
-- Name: analysis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shareinfotosql
--

ALTER SEQUENCE analysis_id_seq OWNED BY analysis.id;


--
-- Name: files; Type: TABLE; Schema: public; Owner: shareinfotosql
--

CREATE TABLE files (
    id bigint NOT NULL,
    paths_id bigint NOT NULL,
    name text NOT NULL,
    folder text NOT NULL,
    size bigint NOT NULL,
    deleted_analysis_id bigint,
    modifitime timestamp without time zone,
    createtime timestamp without time zone,
    found_analysis_id bigint,
    indexed_analysis_id bigint,
    types_id bigint NOT NULL
);


ALTER TABLE files OWNER TO shareinfotosql;

--
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: shareinfotosql
--

CREATE SEQUENCE files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE files_id_seq OWNER TO shareinfotosql;

--
-- Name: files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shareinfotosql
--

ALTER SEQUENCE files_id_seq OWNED BY files.id;


--
-- Name: params; Type: TABLE; Schema: public; Owner: shareinfotosql
--

CREATE TABLE params (
    id bigint NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    paths_id bigint NOT NULL
);


ALTER TABLE params OWNER TO shareinfotosql;

--
-- Name: params_id_seq; Type: SEQUENCE; Schema: public; Owner: shareinfotosql
--

CREATE SEQUENCE params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE params_id_seq OWNER TO shareinfotosql;

--
-- Name: params_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shareinfotosql
--

ALTER SEQUENCE params_id_seq OWNED BY params.id;


--
-- Name: paths; Type: TABLE; Schema: public; Owner: shareinfotosql
--

CREATE TABLE paths (
    id bigint NOT NULL,
    path text NOT NULL
);


ALTER TABLE paths OWNER TO shareinfotosql;

--
-- Name: paths_id_seq; Type: SEQUENCE; Schema: public; Owner: shareinfotosql
--

CREATE SEQUENCE paths_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE paths_id_seq OWNER TO shareinfotosql;

--
-- Name: paths_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shareinfotosql
--

ALTER SEQUENCE paths_id_seq OWNED BY paths.id;


--
-- Name: types; Type: TABLE; Schema: public; Owner: shareinfotosql
--

CREATE TABLE types (
    id bigint NOT NULL,
    name text NOT NULL,
    description text
);


ALTER TABLE types OWNER TO shareinfotosql;

--
-- Name: types_id_seq; Type: SEQUENCE; Schema: public; Owner: shareinfotosql
--

CREATE SEQUENCE types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE types_id_seq OWNER TO shareinfotosql;

--
-- Name: types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: shareinfotosql
--

ALTER SEQUENCE types_id_seq OWNED BY types.id;


--
-- Name: values; Type: TABLE; Schema: public; Owner: shareinfotosql
--

CREATE TABLE "values" (
    id bigint NOT NULL,
    params_id bigint NOT NULL,
    paths_id bigint NOT NULL,
    value text NOT NULL
);


ALTER TABLE "values" OWNER TO shareinfotosql;

--
-- Name: analysis id; Type: DEFAULT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY analysis ALTER COLUMN id SET DEFAULT nextval('analysis_id_seq'::regclass);


--
-- Name: files id; Type: DEFAULT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY files ALTER COLUMN id SET DEFAULT nextval('files_id_seq'::regclass);


--
-- Name: params id; Type: DEFAULT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY params ALTER COLUMN id SET DEFAULT nextval('params_id_seq'::regclass);


--
-- Name: paths id; Type: DEFAULT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY paths ALTER COLUMN id SET DEFAULT nextval('paths_id_seq'::regclass);


--
-- Name: types id; Type: DEFAULT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY types ALTER COLUMN id SET DEFAULT nextval('types_id_seq'::regclass);


--
-- Name: analysis analysis_pkey; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY analysis
    ADD CONSTRAINT analysis_pkey PRIMARY KEY (id);


--
-- Name: files files_pkey; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_pkey PRIMARY KEY (id);


--
-- Name: params params_pkey; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY params
    ADD CONSTRAINT params_pkey PRIMARY KEY (id);


--
-- Name: paths paths_pkey; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY paths
    ADD CONSTRAINT paths_pkey PRIMARY KEY (id);


--
-- Name: types types_pkey; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY types
    ADD CONSTRAINT types_pkey PRIMARY KEY (id);


--
-- Name: analysis unique_analysis_id; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY analysis
    ADD CONSTRAINT unique_analysis_id UNIQUE (id);


--
-- Name: files unique_files_id; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY files
    ADD CONSTRAINT unique_files_id UNIQUE (id);


--
-- Name: files unique_id; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY files
    ADD CONSTRAINT unique_id UNIQUE (id);


--
-- Name: params unique_params_id; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY params
    ADD CONSTRAINT unique_params_id UNIQUE (id);


--
-- Name: paths unique_paths_id1; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY paths
    ADD CONSTRAINT unique_paths_id1 UNIQUE (id);


--
-- Name: types unique_types_id; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY types
    ADD CONSTRAINT unique_types_id UNIQUE (id);


--
-- Name: values unique_values_id; Type: CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY "values"
    ADD CONSTRAINT unique_values_id PRIMARY KEY (id);


--
-- Name: index_deletedtime; Type: INDEX; Schema: public; Owner: shareinfotosql
--

CREATE INDEX index_deletedtime ON public.files USING btree (deleted_analysis_id);


--
-- Name: index_folder; Type: INDEX; Schema: public; Owner: shareinfotosql
--

CREATE INDEX index_folder ON public.files USING btree (folder);


--
-- Name: index_found_indexing_id; Type: INDEX; Schema: public; Owner: shareinfotosql
--

CREATE INDEX index_found_indexing_id ON public.files USING btree (found_analysis_id);


--
-- Name: index_id; Type: INDEX; Schema: public; Owner: shareinfotosql
--

CREATE INDEX index_id ON public.files USING btree (id);


--
-- Name: index_id1; Type: INDEX; Schema: public; Owner: shareinfotosql
--

CREATE INDEX index_id1 ON public.analysis USING btree (id);


--
-- Name: index_indexing_id; Type: INDEX; Schema: public; Owner: shareinfotosql
--

CREATE INDEX index_indexing_id ON public.files USING btree (indexed_analysis_id);


--
-- Name: index_name; Type: INDEX; Schema: public; Owner: shareinfotosql
--

CREATE INDEX index_name ON public.files USING btree (name);


--
-- Name: index_params_id; Type: INDEX; Schema: public; Owner: shareinfotosql
--

CREATE INDEX index_params_id ON public."values" USING btree (params_id);


--
-- Name: index_path; Type: INDEX; Schema: public; Owner: shareinfotosql
--

CREATE INDEX index_path ON public.paths USING btree (path);


--
-- Name: index_paths_id; Type: INDEX; Schema: public; Owner: shareinfotosql
--

CREATE INDEX index_paths_id ON public."values" USING btree (paths_id);


--
-- Name: files lnk_indexing_files; Type: FK CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY files
    ADD CONSTRAINT lnk_indexing_files FOREIGN KEY (found_analysis_id) REFERENCES analysis(id) MATCH FULL;


--
-- Name: files lnk_indexing_files_2; Type: FK CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY files
    ADD CONSTRAINT lnk_indexing_files_2 FOREIGN KEY (indexed_analysis_id) REFERENCES analysis(id) MATCH FULL;


--
-- Name: files lnk_indexing_files_3; Type: FK CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY files
    ADD CONSTRAINT lnk_indexing_files_3 FOREIGN KEY (deleted_analysis_id) REFERENCES analysis(id) MATCH FULL;


--
-- Name: values lnk_params_values; Type: FK CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY "values"
    ADD CONSTRAINT lnk_params_values FOREIGN KEY (params_id) REFERENCES params(id) MATCH FULL;


--
-- Name: files lnk_paths_files; Type: FK CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY files
    ADD CONSTRAINT lnk_paths_files FOREIGN KEY (paths_id) REFERENCES paths(id) MATCH FULL;


--
-- Name: params lnk_paths_params; Type: FK CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY params
    ADD CONSTRAINT lnk_paths_params FOREIGN KEY (paths_id) REFERENCES paths(id) MATCH FULL;


--
-- Name: values lnk_paths_values; Type: FK CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY "values"
    ADD CONSTRAINT lnk_paths_values FOREIGN KEY (paths_id) REFERENCES paths(id) MATCH FULL;


--
-- Name: files lnk_types_files; Type: FK CONSTRAINT; Schema: public; Owner: shareinfotosql
--

ALTER TABLE ONLY files
    ADD CONSTRAINT lnk_types_files FOREIGN KEY (types_id) REFERENCES types(id) MATCH FULL;


--
-- PostgreSQL database dump complete
--

